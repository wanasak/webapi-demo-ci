using System;
using Xunit;

namespace webapi_demo_ci_test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            // Arrange
            var num1 = 1;
            var num2 = 1;

            var expected = 2;

            // Acct
            var actual = num1 + num2;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
